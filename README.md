# Network Manager
This is the coding part result for Groovy Programming Language (assignment is specified [here](https://docs.google.com/document/d/1BSKEwVMWzc0dyqx5zSUb137dKHTIpHMBIvmaW05UgQ4)).
The document with answers to questions placed in Task One and also comment for Task Two can be found [here](https://docs.google.com/document/d/1SxeyaPzpy8F_mvI4usYq8wVNFwSZsCYC3XioRy1Dm5k)
The application has been created using spring-boot and can be built and run without any additional tools (only java is needed).

## Building
Navigate to the application folder (the same as this file) and run following command in the terminal (first run might take longer as it downloads Gradle and Gradle dependencies):
```ssh
./gradlew bootRepackage
```
This builds the application using Gradle - the resulting jar file is located in _build/libs/network-manager-groovy-0.0.1-SNAPSHOT.jar_

## Running
Application can be ran using:
```ssh
java -jar build/libs/network-manager-groovy-0.0.1-SNAPSHOT.jar
```

This starts the application and user is expected to enter commands (typing _exit_ quits the application).

## Usage
The list of possible commands is shown using help command:
```ssh
help
Usage:
<action> [<option>] [<parameters>]

Available commands:
list network - lists all the network elements
list carrier - lists all carriers
list hub <carrier-id> - lists all hubs in carrier specified by id
list node <hub-id> - lists all nodes in hub specified by id
list remedy <element-id> - lists all remedies for the given element (carrier/node/hub)
status network - shows aggregate status of the network
status byName <element-name> - shows aggregate status of network element specified by name
status byId <element-id> - shows aggregate status of network element specified by id
add carrier <carrier-id> <carrier-name> - adds new carrier
add hub <carrier-id> <hub-id> <hub-name> - adds new hub to the carrier specified by id
add node <hub-id> <node-id> <node-name> - adds new node to the hub specified by id
add alarm <element-id> <alarm-code> - adds new alarm to network element (hub/node, doesn't work for carriers), check below for list of supported alarm codes.
remove byId <element-id> - removes the network element specified by id
remove alarm <element-id> <alarm-code> - removes specified alarm from the network element specified by id
remove alarms <element-id>  - removes all alarms from the network element specified by id
rename byId <element-id> <new-name>  - renames the network element specified by id
export json <file-name> - exports the network in json format to the given file
import json <file-name> - imports the network in json format to the given file

Supported alarm codes: [UNIT_UNAVAILABLE, OPTICAL_LOSS, DARK_FIBER, POWER_OUTAGE]
```
Testing network topology can be imported using following command:
```ssh
import json src/test/resources/expected-network.json
```
## Implementation considerations/limitations
There are some comments regarding the implementation and it's limitation in several classes.
The Network class describes limitations of the current model (from architecture/design perspective).
The NetworkManagerService class describes the behavior of the application (as it was understood by me).
