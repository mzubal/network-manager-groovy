package com.errigal.milos.network.manager;

import com.errigal.milos.network.manager.exception.NetworkElementAlreadyExistsException;
import com.errigal.milos.network.manager.model.domain.*;
import com.errigal.milos.network.manager.service.NetworkManagerService;
import com.errigal.milos.network.manager.ui.CLINetworkManager;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NetworkManagerApplicationTests {

	public static final int NUMBER_OF_TESTING_CARRIERS = 3;
	public static final int NUMBER_OF_TESTING_HUBS = 5;
	public static final int NUMBER_OF_TESTING_NODES = 10;
	@Autowired
	NetworkManagerService networkManagerService;

	@Autowired
	CLINetworkManager cliNetworkManager;


	@Before
	public void setup() {
		networkManagerService.clearNetwork();
		createTestingData();
	}

	private void createTestingData() {
		IntStream.rangeClosed(1, NUMBER_OF_TESTING_CARRIERS).forEachOrdered(cn -> {
			String carrierId = "C" + cn;
			networkManagerService.addCarrier(carrierId, "Carrier" + cn);
			IntStream.rangeClosed(1, NUMBER_OF_TESTING_HUBS).forEachOrdered(hn -> {
				String hubId = carrierId + "H" + hn;
				networkManagerService.addHub(carrierId, hubId, "Hub" + hn);
				IntStream.rangeClosed(1, NUMBER_OF_TESTING_NODES).forEachOrdered(nn -> {
					String nodeId = hubId + "N" + nn;
					networkManagerService.addNode(hubId, nodeId, "Node" + nn);
				});
			});
		});
	}

	@Test
	public void checkEmptyNetwork() {
		networkManagerService.clearNetwork();
		Assert.assertEquals(0, networkManagerService.getCarriers().size());
	}

	@Test
	public void checkCarriersCount() {
		Assert.assertEquals(NUMBER_OF_TESTING_CARRIERS, networkManagerService.getCarriers().size());
	}

	@Test(expected = NetworkElementAlreadyExistsException.class)
	public void checkAddCarrierNonUniqueId() {
		networkManagerService.addCarrier("C3", "Totally new name");
	}

	@Test(expected = NetworkElementAlreadyExistsException.class)
	public void checkAddCarrierNonUniqueName() {
		networkManagerService.addCarrier("Totally new Id", "Carrier3");
	}

	@Test(expected = NetworkElementAlreadyExistsException.class)
	public void checkAddHubNonUniqueId() {
		networkManagerService.addHub("C1", "C3H1", "Totally new name");
	}

	@Test(expected = NetworkElementAlreadyExistsException.class)
	public void checkAddHubNonUniqueName() {
		networkManagerService.addHub("C1", "Totally new Id", "Hub1");
	}

	@Test(expected = NetworkElementAlreadyExistsException.class)
	public void checkAddNodeNonUniqueId() {
		networkManagerService.addNode("C1H1", "C3H1N1", "Totally new name");
	}

	@Test(expected = NetworkElementAlreadyExistsException.class)
	public void checkAddNodeNonUniqueName() {
		networkManagerService.addNode("C1H1", "Totally new Id", "Node1");
	}

	@Test
	public void checkAddAlarmsToHub() {
		performAlarmTestForElement("C1H1");
	}

	@Test
	public void checkAddAlarmsToNode() {
		performAlarmTestForElement("C1H1N1");
	}

	// we expect assertion error as alarms cannot be added to carrier
	@Test(expected = AssertionError.class)
	public void checkAddAlarmsToCarrier() {
		performAlarmTestForElement("C1");
	}

	private void performAlarmTestForElement(String testedElementId) {
		Set<Alarm> testingAlarms = createTestingAlarms(testedElementId);
		Set<Alarm> alarms = networkManagerService.getAlarmsForNetworkElementId(testedElementId);
		Assert.assertEquals(4, alarms.size());
		Assert.assertEquals(alarms, testingAlarms);
	}

	private Set<Alarm> createTestingAlarms(String elementId) {
		Set<Alarm> testingAlarms = ImmutableSet.copyOf(Alarm.values());
		// add each possible alarm type to the node
		testingAlarms.forEach(alarm -> networkManagerService.createAlarm(elementId, alarm));
		// add additional alarms
		networkManagerService.createAlarm(elementId, Alarm.UNIT_UNAVAILABLE);
		networkManagerService.createAlarm(elementId, Alarm.POWER_OUTAGE);
		return testingAlarms;
	}

	@Test
	public void checkAlarmRemedies() {
		Arrays.stream(Alarm.values()).forEach(alarm -> {
			Assert.assertNotNull("Alarm Remedy must not be null.", alarm.getRemedyText());
		});
	}

	@Test
	public void checkAlarmStatusOfNodesWhenParentHubIsUnitUnavilable() {
		networkManagerService.createAlarm("C1H1", Alarm.UNIT_UNAVAILABLE);
		networkManagerService.getNodesForHubId("C1H1").stream().forEach(node -> {
			Assert.assertTrue("Node should be UNIT_UNAVAILABLE.", node.getAlarmStatus().contains(Alarm.UNIT_UNAVAILABLE));
		});
	}

	@Test
	public void checkListTheEntireNetworkTheEntireNetwork() {
		Network network = networkManagerService.getNetwork();
		Assert.assertEquals("Count of carriers should be " + NUMBER_OF_TESTING_CARRIERS,
				NUMBER_OF_TESTING_CARRIERS,
				network.getChildren().size());

		Assert.assertEquals("Count of hubs should be " + NUMBER_OF_TESTING_CARRIERS * NUMBER_OF_TESTING_HUBS,
				NUMBER_OF_TESTING_CARRIERS * NUMBER_OF_TESTING_HUBS,
				network.getChildren().stream().flatMap(c -> c.getChildren().stream()).count());

		Assert.assertEquals("Count of nodes should be " + NUMBER_OF_TESTING_CARRIERS * NUMBER_OF_TESTING_HUBS * NUMBER_OF_TESTING_NODES,
				NUMBER_OF_TESTING_CARRIERS * NUMBER_OF_TESTING_HUBS * NUMBER_OF_TESTING_NODES,
				network.getChildren().stream().flatMap(c -> c.getChildren().stream()).flatMap(h -> h.getChildren().stream()).count());

		cliNetworkManager.printNetwork();
	}

	@Test
	public void viewNetworkAlarmStatusWithNoAlarm() {
		Assert.assertEquals("There should be no alarm.", 0, networkManagerService.getAlarms().size());
	}

	@Test
	public void viewNetworkAlarmStatusWithAlarm() {
		networkManagerService.createAlarm("C1H1", Alarm.UNIT_UNAVAILABLE);
		Assert.assertEquals("There should be 1 alarm.", 1, networkManagerService.getAlarms().size());
		Assert.assertTrue("The alarm should be UNIT_UNAVAILABLE.", networkManagerService.getAlarms().contains(Alarm.UNIT_UNAVAILABLE));
	}

	@Test
	public void checkListTheCarriers() {
		Assert.assertEquals("Count of carriers should be " + NUMBER_OF_TESTING_CARRIERS,
				NUMBER_OF_TESTING_CARRIERS,
				networkManagerService.getCarriers().size());
	}

	@Test
	public void checkListTheHubs() {
		Assert.assertEquals("Count of hubs should be " + NUMBER_OF_TESTING_HUBS,
				NUMBER_OF_TESTING_HUBS,
				networkManagerService.getHubsForCarrierId("C1").size());
	}

	@Test
	public void checkListTheNodes() {
		Assert.assertEquals("Count of nodes should be " + NUMBER_OF_TESTING_NODES,
				NUMBER_OF_TESTING_NODES,
				networkManagerService.getNodesForHubId("C1H1").size());
	}

	@Test
	public void checkAlarmStatusOfCarrierByName() throws Exception {
		// create alarm on of the carrier's hub (cannot be created on the carrier itself)
		networkManagerService.createAlarm("C1H1", Alarm.POWER_OUTAGE);
		Assert.assertTrue(
				"Carrier C1 must contain POWER_OUTAGE alarm.",
				networkManagerService.getAlarmsForNetworkElementName("Carrier1").contains(Alarm.POWER_OUTAGE));
	}

	@Test
	public void checkAlarmStatusOfHubByName() throws Exception {
		networkManagerService.createAlarm("C1H1", Alarm.POWER_OUTAGE);
		Assert.assertTrue(
				"Hub C1H1 must contain POWER_OUTAGE alarm.",
				networkManagerService.getAlarmsForNetworkElementName("Hub1").contains(Alarm.POWER_OUTAGE));
	}

	@Test
	public void checkAlarmStatusOfNodeByName() throws Exception {
		networkManagerService.createAlarm("C1H1N1", Alarm.POWER_OUTAGE);
		Assert.assertTrue(
				"Node C1H1N1 must contain POWER_OUTAGE alarm.",
				networkManagerService.getAlarmsForNetworkElementName("Node1").contains(Alarm.POWER_OUTAGE));
	}

	@Test
	public void checkAlarmStatusOfCarrierById() throws Exception {
		// create alarm on of the carrier's hub (cannot be created on the carrier itself)
		networkManagerService.createAlarm("C1H1", Alarm.POWER_OUTAGE);
		Assert.assertTrue(
				"Carrier C1 must contain POWER_OUTAGE alarm.",
				networkManagerService.getAlarmsForNetworkElementId("C1").contains(Alarm.POWER_OUTAGE));
	}

	@Test
	public void checkAlarmStatusOfHubById() throws Exception {
		networkManagerService.createAlarm("C1H1", Alarm.POWER_OUTAGE);
		Assert.assertTrue(
				"Hub C1H1 must contain POWER_OUTAGE alarm.",
				networkManagerService.getAlarmsForNetworkElementId("C1H1").contains(Alarm.POWER_OUTAGE));
	}

	@Test
	public void checkAlarmStatusOfNodeById() throws Exception {
		networkManagerService.createAlarm("C1H1N1", Alarm.POWER_OUTAGE);
		Assert.assertTrue(
				"Node C1H1N1 must contain POWER_OUTAGE alarm.",
				networkManagerService.getAlarmsForNetworkElementId("C1H1N1").contains(Alarm.POWER_OUTAGE));
	}

	@Test
	public void checkAddCarrier() throws Exception {
		String newCarrierId = "NewCarrierId";
		Assert.assertFalse(
				"There should be no Carrier with id " + newCarrierId,
				networkManagerService.getCarriers().stream().map(Carrier::getId).collect(Collectors.toList()).contains(newCarrierId));
		networkManagerService.addCarrier(newCarrierId, "BrandNewCarrier");
		Assert.assertTrue(
				"There should be Carrier with id " + newCarrierId,
				networkManagerService.getCarriers().stream().map(Carrier::getId).collect(Collectors.toList()).contains(newCarrierId));
	}

	@Test
	public void checkRemoveCarrier() throws Exception {
		networkManagerService.removeNetworkElement("C1");
		Assert.assertFalse(
				"There should be no Carrier with id C1.",
				networkManagerService.getCarriers().stream().map(Carrier::getId).collect(Collectors.toList()).contains("C1"));
	}

	@Test
	public void checkAddHub() throws Exception {
		String newHubId = "NewHubId";
		Assert.assertFalse(
				"There should be no Hub with id " + newHubId,
				networkManagerService.getHubsForCarrierId("C1").stream().map(Hub::getId).collect(Collectors.toList()).contains(newHubId));
		networkManagerService.addHub("C1", newHubId, "BrandNewHub");
		Assert.assertTrue(
				"There should be Hub with id " + newHubId,
				networkManagerService.getHubsForCarrierId("C1").stream().map(Hub::getId).collect(Collectors.toList()).contains(newHubId));
	}

	@Test
	public void checkRemoveHub() throws Exception {
		networkManagerService.removeNetworkElement("C1H1");
		Assert.assertFalse(
				"There should be no Hub with id C1H1.",
				networkManagerService.getHubsForCarrierId("C1").stream().map(Hub::getId).collect(Collectors.toList()).contains("C1H1"));
	}

	@Test
	public void checkAddNode() throws Exception {
		String newNodeId = "NewNodeId";
		Assert.assertFalse(
				"There should be no Node with id " + newNodeId,
				networkManagerService.getNodesForHubId("C1H1").stream().map(Node::getId).collect(Collectors.toList()).contains(newNodeId));
		networkManagerService.addNode("C1H1", newNodeId, "BrandNewNode");
		Assert.assertTrue(
				"There should be Node with id " + newNodeId,
				networkManagerService.getNodesForHubId("C1H1").stream().map(Node::getId).collect(Collectors.toList()).contains(newNodeId));
	}

	@Test
	public void checkRemoveNode() throws Exception {
		networkManagerService.removeNetworkElement("C1H1");
		Assert.assertFalse(
				"There should be no Node with id C1H1.",
				networkManagerService.getNodesForHubId("C1H1").stream().map(Node::getId).collect(Collectors.toList()).contains("C1H1"));
	}

	@Test
	public void checkRenameCarrier() throws Exception {
		String newName = "Carrier1-renamed";
		networkManagerService.renameNetworkElement("C1", newName);
		Assert.assertTrue(
				"Renamed element must be present.",
				networkManagerService.getCarriers().stream().map(Carrier::getName).collect(Collectors.toList()).contains(newName));
	}

	@Test(expected = NetworkElementAlreadyExistsException.class)
	public void checkRenameCarrierToExistingName() throws Exception {
		String newName = "Carrier2";
		networkManagerService.renameNetworkElement("C1", newName);
	}

	@Test
	public void checkRenameHub() throws Exception {
		String newName = "Hub1-renamed";
		networkManagerService.renameNetworkElement("C1H1", newName);
		Assert.assertTrue(
				"Renamed element must be present.",
				networkManagerService.getHubsForCarrierId("C1").stream().map(Hub::getName).collect(Collectors.toList()).contains(newName));
	}

	@Test(expected = NetworkElementAlreadyExistsException.class)
	public void checkRenameHubToExistingName() throws Exception {
		String newName = "Hub2";
		networkManagerService.renameNetworkElement("C1H1", newName);
	}

	@Test
	public void checkRenameNode() throws Exception {
		String newName = "Node1-renamed";
		networkManagerService.renameNetworkElement("C1H1N1", newName);
		Assert.assertTrue(
				"Renamed element must be present.",
				networkManagerService.getNodesForHubId("C1H1").stream().map(Node::getName).collect(Collectors.toList()).contains(newName));
	}

	@Test(expected = NetworkElementAlreadyExistsException.class)
	public void checkRenameNodeToExistingName() throws Exception {
		String newName = "Node2";
		networkManagerService.renameNetworkElement("C1H1N1", newName);
	}

	@Test
	public void checkViewRemediesForHub() throws Exception {
		String elementId = "C1H1";
		networkManagerService.createAlarm(elementId, Alarm.UNIT_UNAVAILABLE);
		networkManagerService.getRemediesForNetworkElementId(elementId).contains(Alarm.UNIT_UNAVAILABLE.getRemedyText());
	}

	@Test
	public void checkViewRemediesForNode() throws Exception {
		String elementId = "C1H1N1";
		networkManagerService.createAlarm(elementId, Alarm.UNIT_UNAVAILABLE);
		networkManagerService.getRemediesForNetworkElementId(elementId).contains(Alarm.UNIT_UNAVAILABLE.getRemedyText());
	}

	@Test
	public void checkClearSpecificAlarmOnHub() throws Exception {
		testClearSpecificAlarmOnElement("C1H1");
	}

	@Test
	public void checkClearAllAlarmsOnHub() throws Exception {
		testClearAllAlarmsOnElement("C1H1");
	}

	@Test
	public void checkClearSpecificAlarmOnNode() throws Exception {
		testClearSpecificAlarmOnElement("C1H1N1");
	}

	@Test
	public void checkClearAllAlarmsOnNode() throws Exception {
		testClearAllAlarmsOnElement("C1H1N1");
	}

	private void testClearSpecificAlarmOnElement(String elementId) {
		networkManagerService.createAlarm(elementId, Alarm.UNIT_UNAVAILABLE);
		Assert.assertTrue("The element should contain the alarm.", networkManagerService.getAlarmsForNetworkElementId(elementId).contains(Alarm.UNIT_UNAVAILABLE));
		networkManagerService.removeAlarm(elementId, Alarm.UNIT_UNAVAILABLE);
		Assert.assertFalse("The element should not contain the alarm.", networkManagerService.getAlarmsForNetworkElementId(elementId).contains(Alarm.UNIT_UNAVAILABLE));
	}

	private void testClearAllAlarmsOnElement(String elementId) {
		networkManagerService.createAlarm(elementId, Alarm.UNIT_UNAVAILABLE);
		networkManagerService.createAlarm(elementId, Alarm.POWER_OUTAGE);
		Assert.assertTrue("The element should contain the alarm.", networkManagerService.getAlarmsForNetworkElementId(elementId).contains(Alarm.UNIT_UNAVAILABLE));
		Assert.assertTrue("The element should contain the alarm.", networkManagerService.getAlarmsForNetworkElementId(elementId).contains(Alarm.POWER_OUTAGE));
		networkManagerService.clearAlarms(elementId);
		Assert.assertEquals("The element should not contain any alarm.", 0, networkManagerService.getAlarmsForNetworkElementId(elementId).size());
	}

	@Test
	public void testSerialization() throws Exception {
		networkManagerService.exportToJson("temp-network.json");
		File expected = new File("src/test/resources/expected-network.json");
		File actual = new File("temp-network.json");
		Assert.assertEquals(FileUtils.readLines(expected, "UTF-8"), FileUtils.readLines(actual, "UTF-8"));
	}

	@Test
	public void testDeserialization() throws Exception {
		Network originalNetwork = networkManagerService.getNetwork();
		networkManagerService.importFromJson("src/test/resources/expected-network.json");
		Assert.assertEquals(originalNetwork, networkManagerService.getNetwork());
	}
}
