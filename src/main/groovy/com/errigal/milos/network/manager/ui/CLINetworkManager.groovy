package com.errigal.milos.network.manager.ui

import com.errigal.milos.network.manager.exception.NetworkElementAlreadyExistsException
import com.errigal.milos.network.manager.exception.NetworkManagerRuntimeException
import com.errigal.milos.network.manager.model.domain.Alarm
import com.errigal.milos.network.manager.service.NetworkManagerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import static com.errigal.milos.network.manager.ui.ConsoleUtil.*

/**
 * Sort of an Adapter to transform between console UI (@{@link NetworkManagerCommandExecutor}) and @{@link NetworkManagerService}.
 */
@Component
class CLINetworkManager {

    @Autowired
    NetworkManagerService networkManagerService

    void printNetwork() {
        networkManagerService.getNetwork().getChildren().forEach{ carrier ->
            println entityText(carrier)
            carrier.getChildren().forEach{ hub ->
                println " " * 4 + entityText(hub)
                hub.getChildren().forEach{ node ->
                    println " " * 8 + entityText(node)
                }
            }
        }
    }

    void printNetworkAlarmStatuses() {
        println "Current network statuses: $networkManagerService.getAlarms()"
    }

    void printCarriers() {
        networkManagerService.getCarriers().each { println entityText(it) }
    }

    void printHubsForCarrier(String carrierId) {
        networkManagerService.getHubsForCarrierId(carrierId).each { println entityText(it) }
    }

    void printNodesForHub(String hubId) {
        networkManagerService.getNodesForHubId(hubId).each { println entityText(it) }
    }

    void addCarrier(String id, String name) {
        try {
            networkManagerService.addCarrier(id, name)
            println "Carrier has been created."
        } catch (NetworkManagerRuntimeException e) {
            println "Failed to create carrier: $e.getMessage()"
        }
    }

    void addHub(String carrierId, String id, String name) {
        try {
            networkManagerService.addHub(carrierId, id, name)
            println "Hub has been created."
        } catch (NetworkManagerRuntimeException e) {
            println "Failed to create carrier: $e.getMessage()"
        }
    }

    void addNode(String hubId, String id, String name) {
        try {
            networkManagerService.addNode(hubId, id, name)
            println "Node has been created."
        } catch (NetworkManagerRuntimeException e) {
            println "Failed to create node: $e.getMessage()"
        }
    }

    void removeNetworkElement(String elementId) {
        networkManagerService.removeNetworkElement(elementId)
        println "Removed network element with id $elementId"
    }

    void renameNetworkElement(String elementId, String newName) {
        try {
            networkManagerService.renameNetworkElement(elementId, newName)
            println "Network element with id: $elementId has been renamed to $newName"
        } catch (NetworkElementAlreadyExistsException e) {
            println "Failed to rename element with id: $elementId to name $newName: $e.getMessage()"
        }
    }

    void createAlarm(String elementId, String alarmCode) {
        try {
            networkManagerService.createAlarm(elementId, Alarm.valueOf(alarmCode))
            println "Created Alarm: $alarmCode on element id: $elementId"
        } catch (IllegalArgumentException e) {
            println "$alarmCode is not supported alarm. Choose one of $Alarm.values()"
        }
    }

    void printAlarmStatusOfElementById(String elementId) {
        println "Alarm status of element $elementId is $networkManagerService.getAlarmsForNetworkElementId(elementId)"
    }

    void printAlarmStatusOfElementByName(String elementName) {
        println "Alarm status of element $elementName is ${networkManagerService.getAlarmsForNetworkElementName(elementName)}"
    }

    void printRemediesOfElement(String elementId) {
        println "Remedies for element $elementId are ${networkManagerService.getRemediesForNetworkElementId(elementId)}"
    }

    void clearAlarmForElement(String elementId, String alarmCode) {
        try {
            networkManagerService.removeAlarm(elementId, Alarm.valueOf(alarmCode))
            println "Removed alarm: $alarmCode on element id: $elementId"
        } catch (IllegalArgumentException e) {
            println "$alarmCode is not supported alarm. Choose one of $Alarm.values()"
        }
    }

    void clearAlarmsForElement(String elementId) {
        networkManagerService.clearAlarms(elementId)
    }

    void importNetworkFromFile(String fileName) {
        try {
            networkManagerService.importFromJson(fileName)
            println "Network imported from file $fileName"
        } catch (Exception e) {
            println "Import of network from file $fileName has failed: $e.getMessage()"
        }
    }

    void exportNetworkToFile(String fileName) {
        try {
            networkManagerService.exportToJson(fileName)
            println "Network export to file $fileName"
        } catch (Exception e) {
            println "Export of network to file $fileName has failed: $e.getMessage()"
        }
    }

}
