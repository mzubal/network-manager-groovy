package com.errigal.milos.network.manager.ui;

import com.errigal.milos.network.manager.model.domain.Carrier;
import com.errigal.milos.network.manager.model.domain.Hub;
import com.errigal.milos.network.manager.model.domain.Node;

/**
 * Utility class for printing the model to console.
 */
class ConsoleUtil {

    private ConsoleUtil() {
    }

    static String entityText(Carrier carrier) {
        "Carrier - name: $carrier.name id: $carrier.id alarms: $carrier.alarmStatus"
    }

    static String entityText(Hub hub) {
        "Hub - name: $hub.name id: $hub.id alarms: $hub.alarmStatus"
    }

    static String entityText(Node node) {
        "Node - name: $node.name id: $node.id alarms: $node.alarmStatus"
    }

}