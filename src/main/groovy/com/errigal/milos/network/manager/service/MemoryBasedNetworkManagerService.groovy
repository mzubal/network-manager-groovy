package com.errigal.milos.network.manager.service;

import com.errigal.milos.network.manager.exception.MultipleNetworkElementsFound;
import com.errigal.milos.network.manager.exception.NetworkElementAlreadyExistsException;
import com.errigal.milos.network.manager.exception.NetworkElementNotFound;
import com.errigal.milos.network.manager.exception.SerializationException;
import com.errigal.milos.network.manager.model.base.*;
import com.errigal.milos.network.manager.model.domain.*;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Memory-based implementation of the @{@link NetworkManagerService}.
 * It uses in-memory object to hold the network model.
 * In a bigger project I would consider separating serialization to a different service as it is storage agnostic.
 */
@Component
class MemoryBasedNetworkManagerService implements NetworkManagerService {

    Network network = Network.create()
    ObjectMapper jsonMapper = new ObjectMapper()

    MemoryBasedNetworkManagerService() {
        jsonMapper.setVisibility(jsonMapper.getSerializationConfig().getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withCreatorVisibility(JsonAutoDetect.Visibility.NONE))
    }

    @Override
    Network getNetwork() {
        return network
    }

    @Override
    void clearNetwork() {
        network = Network.create()
    }

    @Override
    Set<Alarm> getAlarms() {
        return network.getAlarmStatus()
    }

    @Override
    List<Carrier> getCarriers() {
        return network.getChildren()
    }

    @Override
    List<Hub> getHubsForCarrierId(String carrierId) {
        findChildren {it.getClass() == Carrier && it.id == carrierId}
    }

    @Override
    List<Node> getNodesForHubId(String hubId) {
        findChildren {it.getClass() == Hub && it.id == hubId}
    }

    @Override
    Set<Alarm> getAlarmsForNetworkElementName(String name) {
        findElements { it.name == name } collect { it.getAlarmStatus() } flatten()
    }

    @Override
    Set<Alarm> getAlarmsForNetworkElementId(String id) {
        findElements { it.id == id } collect { it.getAlarmStatus() } flatten()
    }

    @Override
    synchronized void addCarrier(String id, String name) {
        if (findElements {it.getClass() == Carrier && (it.id == id || it.name == name)}.size() < 1) {
            network.addChild(Carrier.create(name, id, network))
        } else {
            throw new NetworkElementAlreadyExistsException(id, name)
        }
    }

    @Override
    synchronized void addHub(String parentCarrierId, String id, String name) {
        List<Carrier> foundCarriers = findElements { it.getClass() == Carrier && it.id == parentCarrierId }
        if (foundCarriers.size() == 0) {
            throw new NetworkElementNotFound(parentCarrierId)
        } else if (foundCarriers.size() == 1) {
            Carrier parentCarrier = foundCarriers.get(0)
            // Find count of all hubs with either the same id or with the same name in scope of parent carrier
            Long duplicateHubsCount = findElements { it.getClass() == Hub && (it.id == id || (it.name == name && it.parent.id == parentCarrierId))}.size()
            if (duplicateHubsCount < 1) {
                parentCarrier.addChild(Hub.create(name, id, parentCarrier))
            } else {
                throw new NetworkElementAlreadyExistsException(id, name)
            }
        } else if (foundCarriers.size() > 1) {
            throw new MultipleNetworkElementsFound(parentCarrierId)
        }
    }

    @Override
    synchronized void addNode(String parentHubId, String id, String name) {
        List<Hub> foundHubs = findElements { it.getClass() == Hub && it.id == parentHubId }
        if (foundHubs.size() == 0) {
            throw new NetworkElementNotFound(parentHubId)
        } else if (foundHubs.size() == 1) {
            Hub parentHub = foundHubs.get(0)
            // Find count of all nodes with either the same id or with the same name in scope of parent hub
            Long duplicateNodeCount = findElements { it.getClass() == Node && (it.id == id || (it.name == name && it.parent.id == parentHubId))}.size()
            if (duplicateNodeCount < 1) {
                parentHub.addChild(Node.create(name, id, parentHub))
            } else {
                throw new NetworkElementAlreadyExistsException(id, name)
            }
        } else if (foundHubs.size() > 1) {
            throw new MultipleNetworkElementsFound(parentHubId)
        }
    }

    @Override
    void removeNetworkElement(String id) {
        findElements { it.id == id }.each { it.remove() }
    }

    @Override
    void renameNetworkElement(String id, String newName) {
        findElements {it.id == id} each { element ->
            def sameNamedSiblingsCount = findElements {it.parent == element.parent}.findAll{it.name == newName}.size()
            if (sameNamedSiblingsCount < 1) {
                element.setName(newName)
            } else {
                throw new NetworkElementAlreadyExistsException(id, newName)
            }
        }
    }

    @Override
    void createAlarm(String id, Alarm newAlarm) {
        findElements {it instanceof AlarmHolder && it.id == id}.each{ it.addAlarm(newAlarm) }
    }

    @Override
    Set<String> getRemediesForNetworkElementId(String id) {
        findElements {it instanceof AlarmHolder && it.id == id} collect {it.getAlarmStatus()} flatten {it.remedyText}
    }

    @Override
    void removeAlarm(String id, Alarm alarmToRemove) {
        findElements {it instanceof AlarmHolder && it.id == id} each {it.clearAlarm(alarmToRemove)}
    }

    @Override
    void clearAlarms(String id) {
        findElements {it instanceof AlarmHolder && it.id == id} each {it.clearAlarms()}
    }

    @Override
    void importFromJson(String fileName) {
        try {
            network = jsonMapper.readValue(new File(fileName), Network.class)
        } catch (IOException e) {
            throw new SerializationException(e)
        }
    }

    @Override
    void exportToJson(String fileName) {
        try {
            jsonMapper.writeValue(new File(fileName), network)
        } catch (IOException e) {
            throw new SerializationException(e)
        }
    }

    def findElements(Closure filter) {
        (
            network.children + network.children.collect { carrier ->
                carrier.children + carrier.children.collect { hub ->
                    hub.children
                }.flatten()
            }.flatten()
        )
        .findAll (filter)
    }

    def findChildren(Closure filter) {
        findElements(filter) collect { e -> e.children } flatten()
    }

}
