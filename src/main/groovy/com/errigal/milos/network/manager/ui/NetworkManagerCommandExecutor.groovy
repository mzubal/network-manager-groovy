package com.errigal.milos.network.manager.ui

import com.errigal.milos.network.manager.model.domain.Alarm
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import java.util.*

/**
 * Class implementing the command line UI.
 * It provides single method #startCommandLoop, which reads lines typed by the user and performs the desired actions.
 */
@Component
class NetworkManagerCommandExecutor {

    public static final String UNKNOWN_COMMAND_MESSAGE = "Unrecognized command, option or parameters - use help command to show list of supported commands."

    @Autowired
    CLINetworkManager cliNetworkManager

    void startCommandLoop() {
        boolean continueCommandLoop = true
        Scanner scanner = new Scanner(System.in)
        println "The network-manager is ready accept commands now."
        while (continueCommandLoop && scanner.hasNextLine()) {
            try {
                String[] lineWords = scanner.nextLine().split(" ")
                String command = ""
                String option = ""
                List<String> params = new ArrayList<>()

                for (int i = 0 ; i < lineWords.length; i++) {
                    if (i == 0) {
                        command = lineWords[0]
                    } else if (i == 1) {
                        option = lineWords[1]
                    } else {
                        params.add(lineWords[i])
                    }
                }

                switch (command) {
                    case "list" :
                        switch (option) {
                            case "network" : cliNetworkManager.printNetwork(); break
                            case "carrier" : cliNetworkManager.printCarriers(); break
                            case "hub" : cliNetworkManager.printHubsForCarrier(params.get(0)); break
                            case "node" : cliNetworkManager.printNodesForHub(params.get(0)); break
                            case "remedy" : cliNetworkManager.printRemediesOfElement(params.get(0)); break
                            default: println UNKNOWN_COMMAND_MESSAGE
                        }
                        break
                    case "status" :
                        switch (option) {
                            case "network" : cliNetworkManager.printNetworkAlarmStatuses(); break
                            case "byName" : cliNetworkManager.printAlarmStatusOfElementByName(params.get(0)); break
                            case "byId" : cliNetworkManager.printAlarmStatusOfElementById(params.get(0)); break
                            default: println UNKNOWN_COMMAND_MESSAGE
                        }
                        break
                    case "add" :
                        switch (option) {
                            case "carrier" : cliNetworkManager.addCarrier(params.get(0), params.get(1)); break
                            case "hub" : cliNetworkManager.addHub(params.get(0), params.get(1), params.get(2)); break
                            case "node" : cliNetworkManager.addNode(params.get(0), params.get(1), params.get(2)); break
                            case "alarm" : cliNetworkManager.createAlarm(params.get(0), params.get(1)); break
                            default: println UNKNOWN_COMMAND_MESSAGE
                        }
                        break
                    case "remove" :
                        switch (option) {
                            case "byId" : cliNetworkManager.removeNetworkElement(params.get(0)); break
                            case "alarm" : cliNetworkManager.clearAlarmForElement(params.get(0), params.get(1)); break
                            case "alarms" : cliNetworkManager.clearAlarmsForElement(params.get(0)); break
                            default: println UNKNOWN_COMMAND_MESSAGE
                        }
                        break
                    case "rename" :
                        switch (option) {
                            case "byId" : cliNetworkManager.renameNetworkElement(params.get(0), params.get(1)); break
                            default: println UNKNOWN_COMMAND_MESSAGE
                        }
                        break
                    case "export" :
                        switch (option) {
                            case "json" : cliNetworkManager.exportNetworkToFile(params.get(0)); break
                            default: println UNKNOWN_COMMAND_MESSAGE
                        }
                        break
                    case "import" :
                        switch (option) {
                            case "json" : cliNetworkManager.importNetworkFromFile(params.get(0)); break
                            default: println UNKNOWN_COMMAND_MESSAGE
                        }
                        break
                    case "help" : printHelp(); break
                    case "exit" : continueCommandLoop = false; break
                    default:
                        println UNKNOWN_COMMAND_MESSAGE
                }
            } catch (NoSuchElementException) {
                println UNKNOWN_COMMAND_MESSAGE
                scanner.nextLine()
            } catch (IndexOutOfBoundsException) {
                println UNKNOWN_COMMAND_MESSAGE
            }
        }
    }

    private void printHelp() {
        println "Usage:"
        println "<action> [<option>] [<parameters>]"
        println ""
        println "Available commands:"
        println "list network - lists all the network elements"
        println "list carrier - lists all carriers"
        println "list hub <carrier-id> - lists all hubs in carrier specified by id"
        println "list node <hub-id> - lists all nodes in hub specified by id"
        println "list remedy <element-id> - lists all remedies for the given element (carrier/node/hub)"
        println "status network - shows aggregate status of the network"
        println "status byName <element-name> - shows aggregate status of network element specified by name"
        println "status byId <element-id> - shows aggregate status of network element specified by id"
        println "add carrier <carrier-id> <carrier-name> - adds new carrier"
        println "add hub <carrier-id> <hub-id> <hub-name> - adds new hub to the carrier specified by id"
        println "add node <hub-id> <node-id> <node-name> - adds new node to the hub specified by id"
        println "add alarm <element-id> <alarm-code> - adds new alarm to network element (hub/node, doesn't work for carriers), check below for list of supported alarm codes."
        println "remove byId <element-id> - removes the network element specified by id"
        println "remove alarm <element-id> <alarm-code> - removes specified alarm from the network element specified by id"
        println "remove alarms <element-id>  - removes all alarms from the network element specified by id"
        println "rename byId <element-id> <new-name>  - renames the network element specified by id"
        println "export json <file-name> - exports the network in json format to the given file"
        println "import json <file-name> - imports the network in json format to the given file"
        println "help - prints this help"
        println "exit - exits the application"
        println ""
        println "Supported alarm codes: ${Arrays.toString(Alarm.values())}"
    }
}
