package com.errigal.milos

import com.errigal.milos.network.manager.ui.NetworkManagerCommandExecutor
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.ApplicationContext

@SpringBootApplication
class NetworkManagerGroovyApplication {

	static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(NetworkManagerGroovyApplication, args)
		// Retrieve the command executor from context and start the command loop
		NetworkManagerCommandExecutor commandExecutor = context.getBean NetworkManagerCommandExecutor
		commandExecutor.startCommandLoop()
	}
}
