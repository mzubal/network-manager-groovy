package com.errigal.milos.network.manager.model.domain;

import com.errigal.milos.network.manager.model.base.AlarmStatusAware;
import com.errigal.milos.network.manager.model.base.ChildrenAware;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Class representing the network model.
 * The model is shared for storage, viewing and serialization
 * (it would be worth separating the models for each of these concerns in a bigger application).
 * The model uses @{@link JsonManagedReference} and @{@link com.fasterxml.jackson.annotation.JsonBackReference} annotations
 * to prevent the serializer (which is using Jackson library) from infinite loops on back references in the tree.
 */
public class Network implements AlarmStatusAware, ChildrenAware<Carrier> {

    @JsonManagedReference
    private List<Carrier> carriers = new ArrayList<>();

    @JsonCreator
    public static Network create() {
        return new Network();
    }

    @Override
    public List<Carrier> getChildren() {
        return carriers;
    }

    @Override
    public Set<Alarm> getAlarmStatus() {
        return carriers.stream()
                .flatMap(carrier -> carrier.getAlarmStatus().stream())
                .collect(Collectors.toSet());
    }

    @Override
    public void addChild(Carrier entityToAdd) {
        carriers.add(entityToAdd);
    }

    @Override
    public void removeChild(Carrier entityToRemove) {
        carriers.remove(entityToRemove);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Network network = (Network) o;

        return carriers.equals(network.carriers);
    }

    @Override
    public int hashCode() {
        return carriers.hashCode();
    }
}
