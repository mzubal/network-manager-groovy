package com.errigal.milos.network.manager.model.domain;

import com.errigal.milos.network.manager.model.base.AlarmHolder;
import com.errigal.milos.network.manager.model.base.ParentAware;
import com.errigal.milos.network.manager.model.base.Removable;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Class representing the Node.
 */
public class Node extends AlarmHolder implements ParentAware<Hub>, Removable {

    @JsonBackReference
    private Hub parentHub;

    private Node(String name, String id, Hub parentHub) {
        super(name, id);
        this.parentHub = parentHub;
    }

    public static Node create(String name, String id, Hub parentHub) {
        return new Node(name, id, parentHub);
    }

    @JsonCreator
    public static Node create(@JsonProperty("name") String name, @JsonProperty("id") String id) {
        return new Node(name, id, null);
    }

    @Override
    public Hub getParent() {
        return parentHub;
    }

    @Override
    public void remove() {
        getParent().removeChild(this);
    }

    @Override
    public Set<Alarm> getAlarmStatus() {
        Set<Alarm> alarms = new LinkedHashSet<>(super.getAlarmStatus());
        // add UNIT_UNAVAILABLE alarms to current alarms
        alarms.addAll(getParent().getAlarmStatus().stream().filter(a -> a.equals(Alarm.UNIT_UNAVAILABLE)).collect(Collectors.toSet()));
        return alarms;
    }
}
