package com.errigal.milos.network.manager.model.base;

/**
 * Basic interface to be implemented by model classes, which can be removed from the model.
 */
public interface Removable {

    void remove();

}
