package com.errigal.milos.network.manager.exception;

/**
 * This exception is thrown in case newly added or renamed network element conflicts with other elements (by having the same name).
 */
public class NetworkElementAlreadyExistsException extends NetworkManagerRuntimeException {

    public NetworkElementAlreadyExistsException(String elementId) {
        super("Network element with id: " + elementId + " already exists.");
    }

    public NetworkElementAlreadyExistsException(String elementId, String elementName) {
        super("Network element with id: " + elementId + " or name: " + elementName + " already exists.");
    }

}
