package com.errigal.milos.network.manager.exception;

/**
 * This exception is thrown in cases the desired network element is not found (e.g. when referencing a carrier while adding new hub).
 */
public class NetworkElementNotFound extends NetworkManagerRuntimeException {

    public NetworkElementNotFound(String elementId) {
        super("Network element with id: " + elementId + " was not found.");
    }

}
