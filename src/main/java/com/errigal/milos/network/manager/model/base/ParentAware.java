package com.errigal.milos.network.manager.model.base;

/**
 * Basic interface to be implemented by all model classes, which are supposed have a parent in the hierarchy.
 */
public interface ParentAware<T extends ChildrenAware> {

    /**
     * Returns the parent in the hierarchy.
     * @return @{@link ParentAware}
     */
    T getParent();

}
