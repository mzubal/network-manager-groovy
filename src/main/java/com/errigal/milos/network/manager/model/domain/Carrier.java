package com.errigal.milos.network.manager.model.domain;

import com.errigal.milos.network.manager.model.base.*;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Class representing the Carrier.
 */
public class Carrier extends IdentifiedAndNamedEntity implements AlarmStatusAware, ParentAware<Network>, ChildrenAware<Hub>, Removable {

    @JsonBackReference
    private Network parentNetwork;

    @JsonManagedReference
    private List<Hub> hubs = new ArrayList<>();

    private Carrier(String name, String id, Network parentNetwork) {
        super(name, id);
        this.parentNetwork = parentNetwork;
    }

    public static Carrier create(String name, String id, Network parentNetwork) {
        return new Carrier(name, id, parentNetwork);
    }

    @JsonCreator
    public static Carrier create(@JsonProperty("name") String name, @JsonProperty("id") String id) {
        return new Carrier(name, id, null);
    }

    @Override
    public List<Hub> getChildren() {
        return hubs;
    }

    @Override
    public void addChild(Hub entityToAdd) {
        hubs.add(entityToAdd);
    }

    @Override
    public void removeChild(Hub entityToRemove) {
        hubs.remove(entityToRemove);
    }

    @Override
    public Set<Alarm> getAlarmStatus() {
        return hubs.stream()
                .flatMap(hub -> hub.getAlarmStatus().stream())
                .collect(Collectors.toSet());
    }

    @Override
    @JsonBackReference
    public Network getParent() {
        return parentNetwork;
    }

    @Override
    public void remove() {
        getParent().removeChild(this);
    }
}
