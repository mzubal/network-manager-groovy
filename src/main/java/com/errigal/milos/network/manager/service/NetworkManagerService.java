package com.errigal.milos.network.manager.service;

import com.errigal.milos.network.manager.exception.NetworkElementAlreadyExistsException;
import com.errigal.milos.network.manager.model.domain.*;

import java.util.List;
import java.util.Set;

/**
 * The main API defining operation over the @{@link Network}.
 * The API mixes different concerns (creation, queries, serialization), which would be better to be separated for a bigger application.
 */
public interface NetworkManagerService {

    /**
     * Return the current network.
     * @return Current network model (@{@link Network})
     */
    Network getNetwork();

    /**
     * Clears the whole network.
     */
    void clearNetwork();

    /**
     * Returns set of alarm types present in the network.
     * E.g. if any network element contains alarm UNIT_UNAVAILABLE,
     * that state is returned in the set.
     * @return set of @{@link Alarm}
     */
    Set<Alarm> getAlarms();

    /**
     * Returns all carriers in the network. Empty list is returned in case there are no carriers.
     * @return list of @{@link Carrier}
     */
    List<Carrier> getCarriers();

    /**
     * Returns the hubs for the given carrier id. Returns empty list in case the carrier is not present.
     * @param carrierId - id of the carrier to list the hubs for
     * @return list of @{@link Hub}
     */
    List<Hub> getHubsForCarrierId(String carrierId);

    /**
     * Returns the nodes for the given hub id. Returns empty list in case the hub is not present.
     * @param hubId - id of the hub to list the nodes for
     * @return list of @{@link Node}
     */
    List<Node> getNodesForHubId(String hubId);

    /**
     * Returns the set of alarms for the given network element name.
     * Returns empty set in case there is no element with such name.
     * Returns statuses for all elements with the given name (in case there are multiple ones).
     *
     * @param name - name of the network element
     * @return set of @{@link Alarm}
     */
    Set<Alarm> getAlarmsForNetworkElementName(String name);

    /**
     * Returns the set of alarms for the given network element id.
     * Returns empty set in case there is no element with such id.
     *
     * @param id - id of the network element
     * @return set of @{@link Alarm}
     */
    Set<Alarm> getAlarmsForNetworkElementId(String id);

    /**
     * Adds new carrier to the network.
     * Both carrier id and name must be unique within network.
     * @param id
     * @param name
     * @throws @{@link NetworkElementAlreadyExistsException} in case the carrier already exists
     */
    void addCarrier(String id, String name);

    /**
     * Adds new hub to the given carrier.
     * The hub id must be unique within the network.
     * @param parentCarrierId - the id of parent @{@link Carrier}
     * @param id
     * @param name
     * @throws @{@link com.errigal.milos.network.manager.exception.NetworkElementNotFound} in case the carrier with the id doesn't exist
     * @throws @{@link NetworkElementAlreadyExistsException} in case the hub already exists
     */
    void addHub(String parentCarrierId, String id, String name);

    /**
     * Adds new node to the given hub.
     *
     * @param parentHubId - the parent @{@link Hub}
     * @param id
     * @param name
     * @throws @{@link com.errigal.milos.network.manager.exception.NetworkElementNotFound} in case the hub with the id doesn't exist
     * @throws @{@link NetworkElementAlreadyExistsException} in case the node already exists
     */
    void addNode(String parentHubId, String id, String name);

    /**
     * Remove the given element from network.
     * Nothing happens in case the element doesn't exist.
     * @param id - id of the network element to be removed
     */
    void removeNetworkElement(String id);

    /**
     * Rename the given element in the network.
     * @param id - id of the network element to be renamed
     * @throws @{@link com.errigal.milos.network.manager.exception.NetworkElementNotFound} in case the element
     */
    void renameNetworkElement(String id, String newName);

    /**
     * Create alarm on the given hub/node
     * @param id - id of the hub/node to add the new alarm to
     * @param newAlarm - the new @{@link Alarm} to be added
     * @throws @{@link com.errigal.milos.network.manager.exception.NetworkElementNotFound} in case the hub/node with the id doesn't exist
     */
    void createAlarm(String id, Alarm newAlarm);

    /**
     * Returns the set of remedies for the given hub/node.
     * Returns nothing in case the hub/node doesn't exist.
     * @param id - id of the hub/node to get the remedies for
     * @return - set of remedies (texts)
     */
    Set<String> getRemediesForNetworkElementId(String id);

    /**
     * Remove alarm from the given hub/node id.
     * @param id - id of the hub/node to remove the alarm from
     * @param alarmToRemove - the @{@link Alarm} to be removed
     * @throws @{@link com.errigal.milos.network.manager.exception.NetworkElementNotFound} in case the hub/node with the id doesn't exist
     */
    void removeAlarm(String id, Alarm alarmToRemove);

    /**
     * Clears all alarms for the given hub/node.
     * @param id - id of the hub/node to clear the alarms
     */
    void clearAlarms(String id);

    /**
     * Loads the network model in JSON format from a given file.
     * @param fileName - file name of the json serialized network model
     */
    void importFromJson(String fileName);

    /**
     * Stores the network model to a given file in JSON format.
     * @param fileName - file name of the file to store the model to
     */
    void exportToJson(String fileName);

}
