package com.errigal.milos.network.manager.model.domain;

/**
 * Basic enum to represent alarms. It is intentionally very simple - all the texts are "static".
 */
public enum Alarm {

    UNIT_UNAVAILABLE("The unit is not available.", "Check if it really exists!"),
    OPTICAL_LOSS("There is optical loss.", "Make the fibers shorter!"),
    DARK_FIBER("The unit is not used.", "Make use of the unit or get rid off it!"),
    POWER_OUTAGE("There is no power.", "Turn the power on!")
    ;

    private String alarmText = "";
    private String remedyText = "";

    Alarm(String alarmText, String remedyText) {
        this.alarmText = alarmText;
        this.remedyText = remedyText;
    }

    public String getAlarmText() {
        return alarmText;
    }

    public String getRemedyText() {
        return remedyText;
    }

}
