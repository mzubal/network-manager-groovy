package com.errigal.milos.network.manager.model.domain;

import com.errigal.milos.network.manager.model.base.AlarmHolder;
import com.errigal.milos.network.manager.model.base.ChildrenAware;
import com.errigal.milos.network.manager.model.base.ParentAware;
import com.errigal.milos.network.manager.model.base.Removable;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Class represnting the Hub.
 */
public class Hub extends AlarmHolder implements ParentAware<Carrier>, ChildrenAware<Node>, Removable {

    @JsonBackReference
    private Carrier parentCarrier;

    @JsonManagedReference
    private List<Node> nodes = new ArrayList<>();

    private Hub(String name, String id, Carrier parentCarrier) {
        super(name, id);
        this.parentCarrier = parentCarrier;
    }

    public static Hub create(String name, String id, Carrier parentCarrier) {
        return new Hub(name, id, parentCarrier);
    }

    @JsonCreator
    public static Hub create(@JsonProperty("name") String name, @JsonProperty("id") String id) {
        return new Hub(name, id, null);
    }

    @Override
    public void addChild(Node entityToAdd) {
        nodes.add(entityToAdd);
    }

    @Override
    public void removeChild(Node entityToRemove) {
        nodes.remove(entityToRemove);
    }

    @Override
    public Carrier getParent() {
        return parentCarrier;
    }

    @Override
    public void remove() {
        getParent().removeChild(this);
    }

    @Override
    public List<Node> getChildren() {
        return nodes;
    }
}
