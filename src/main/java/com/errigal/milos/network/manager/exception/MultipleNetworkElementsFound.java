package com.errigal.milos.network.manager.exception;

/**
 * Exception to be thrown in case there multiple elements with the same id in the scope of whole network.
 * This should not ever happen under normal circumstances.
 */
public class MultipleNetworkElementsFound extends NetworkManagerRuntimeException {

    public MultipleNetworkElementsFound(String elementId) {
        super("Multiple network elements for id: " + elementId + " were found.");
    }

}
