package com.errigal.milos.network.manager.model.base;

/**
 * Base class for entities with id and name.
 */
public abstract class IdentifiedAndNamedEntity implements Comparable<IdentifiedAndNamedEntity> {

    protected String id;
    protected String name;

    protected IdentifiedAndNamedEntity(String name, String id) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdentifiedAndNamedEntity that = (IdentifiedAndNamedEntity) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "IdentifiedAndNamedEntity{" +
                "id='" + id + '\'' +
                '}';
    }

    @Override
    public int compareTo(IdentifiedAndNamedEntity otherEntity) {
        return this.getName().compareTo(otherEntity.getName());
    }
}
