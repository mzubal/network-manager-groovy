package com.errigal.milos.network.manager.exception;

/**
 * Base for all application-specific exceptions.
 */
public class NetworkManagerRuntimeException extends RuntimeException {

    public NetworkManagerRuntimeException(String message) {
        super(message);
    }

    public NetworkManagerRuntimeException(Throwable cause) {
        super(cause);
    }
}
