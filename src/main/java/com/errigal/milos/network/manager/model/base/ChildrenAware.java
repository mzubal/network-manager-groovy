package com.errigal.milos.network.manager.model.base;

import java.util.List;

/**
 * Basic interface to implemented by model classes, which are supposed to have children entities.
 */
public interface ChildrenAware<T extends Removable> {

    /**
     * Returns the list of children.
     * @return list of children
     */
    List<T> getChildren();

    /**
     * Adds the entity to a children collection.
     * @param entityToAdd
     */
    void addChild(T entityToAdd);

    /**
     * Removes the entity from children collection.
     * @param entityToRemove
     */
    void removeChild(T entityToRemove);

}
