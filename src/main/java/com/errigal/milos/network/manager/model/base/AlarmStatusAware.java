package com.errigal.milos.network.manager.model.base;

import com.errigal.milos.network.manager.model.domain.Alarm;

import java.util.Set;

/**
 * Interface to be implemented by model classes, which are supposed to return their alarm status,
 * but they don't necessarily need to be holding alarm status themselves.
 * Use @{@link AlarmHolder} in order to create new entity type which actually holds the alarms.
 */
public interface AlarmStatusAware {

    /**
     * The list of alarms the particular entity has.
     * @return list of @{@link Alarm}
     */
    Set<Alarm> getAlarmStatus();

}
