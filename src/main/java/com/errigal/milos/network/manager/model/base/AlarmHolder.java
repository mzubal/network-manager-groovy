package com.errigal.milos.network.manager.model.base;


import com.errigal.milos.network.manager.model.domain.Alarm;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Base class to be used by all entities which hold the alarms.
 * In case the entity doesn't hold alarm, but wants to expose aggregation of it's children alarms,
 * it should implement @{@link AlarmStatusAware}
 */
public abstract class AlarmHolder extends IdentifiedAndNamedEntity implements AlarmStatusAware {

    private List<Alarm> alarms = new ArrayList<>();

    protected AlarmHolder(String name, String id) {
        super(name, id);
    }

    @Override
    public Set<Alarm> getAlarmStatus() {
        return new LinkedHashSet<>(alarms);
    }

    public void addAlarm(Alarm alarm) {
        alarms.add(alarm);
    }

    public void clearAlarm(Alarm alarm) {
        alarms.remove(alarm);
    }

    public void clearAlarms() {
        alarms.clear();
    }

}
