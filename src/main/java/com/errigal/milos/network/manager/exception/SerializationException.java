package com.errigal.milos.network.manager.exception;

/**
 * Specific exception to be thrown in case of problems with serialization/deserialization.
 */
public class SerializationException extends NetworkManagerRuntimeException {

    public SerializationException(Throwable e) {
        super(e);
    }

}
